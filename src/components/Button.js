import * as R from 'ramda'
import React from 'react'
import styled from '@emotion/styled'
import styledPipe, {
  detectSizes,
  detectWeights,
  detectRadius,
  detectDisabled,
  TYPES,
  on,
  modifyStyle,
  propsHave,
  ifdo,
  propContain,
  replaceStyles,
} from '../styles/styledPipe'
import {fc, bgc, bc} from '../styles/utils/colors'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faSpinner} from '@fortawesome/free-solid-svg-icons'
import {callTo} from '../utils/functions'
import {darken} from '../styles/utils/helpers'
import {w} from '../styles/utils/sizes'

const detectColorsSchema = propsHave(callTo(bgc, bc), 'primary', TYPES)

const detectActions = [
  on(modifyStyle(darken(R.__, 0.05), 'backgroundColor'), 'hover'),
  on(modifyStyle(darken(R.__, 0.1), 'backgroundColor'), 'active'),
  on(modifyStyle(darken(R.__, 0.07), 'backgroundColor'), 'focus'),
]

const detectInvert = ifdo(
  propContain('invert'),
  replaceStyles('backgroundColor', 'color'),
)

const detectBlock = ifdo(propContain('block'), w('100%'))

const ButtonStyle = styled.button(
  styledPipe(
    [{border: '1px solid #fff', cursor: 'pointer'}, fc('white')],
    detectSizes,
    detectColorsSchema,
    detectInvert,
    detectActions,
    detectDisabled,
    detectWeights,
    detectRadius,
    detectBlock,
  ),
)

function Button({children, loading = false, ...props}) {
  return (
    <ButtonStyle disabled={loading} {...props}>
      {children} {loading && <FontAwesomeIcon icon={faSpinner} spin />}
    </ButtonStyle>
  )
}

export default Button
