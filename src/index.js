import React from 'react'
import {render} from 'react-dom'
import ThemeProvider from './styles/theme/ThemeProvider'
import Button from './components/Button'

class App extends React.Component {
  state = {}
  render() {
    return <Button>Hola</Button>
  }
}

render(
  <ThemeProvider>
    <App />
  </ThemeProvider>,
  document.getElementById('app'),
)
