import * as R from 'ramda'

export const isArray = R.compose(
  R.equals('Array'),
  R.type,
)
export const isObject = R.compose(
  R.equals('Object'),
  R.type,
)
export const isFunction = R.compose(
  R.equals('Function'),
  R.type,
)
export const isString = R.compose(
  R.equals('String'),
  R.type,
)

export const ifFunctionCallWith = (fn, ...args) =>
  isFunction(fn) ? fn(...args) : fn

export const pickAllOmit = (keys, obj) => [R.pick(keys, obj), R.omit(keys, obj)]

export const mapToKeys = (fn, arr) =>
  arr.reduce((acc, i) => ({...acc, [fn(i)]: i}), {})

export function Event() {
  const fnsArr = []
  return {
    sub(...fns) {
      fnsArr.push(...fns)
    },
    pup(...args) {
      fnsArr.forEach(fn => fn(...args))
    },
  }
}

export function functionMemo() {
  let fn = () => {}
  return {
    handler(...args) {
      fn(...args)
    },
    save(newFn) {
      fn = newFn
    },
  }
}

export const callTo = (...fns) => arg => fns.map(fn => fn(arg))
