import {darken} from '../utils/helpers'

export const primary = '#33a8ff'
export const secondary = '#fb9410'
export const danger = '#d32f2f'
export const success = '#7CB342'
export const info = '#5c9ec0'
export const warning = '#FFB300'
export const error = danger

// const log = a => {
//   console.log(a)
//   return a
// }

export const black = '#000000'
export const white = '#ffffff'
export const gray1 = darken(white, 0.1)
export const gray2 = darken(white, 0.2)
export const gray3 = darken(white, 0.3)
export const gray4 = darken(white, 0.4)
export const gray5 = darken(white, 0.5)
export const gray6 = darken(white, 0.6)
export const gray7 = darken(white, 0.7)
export const gray8 = darken(white, 0.8)
export const gray9 = darken(white, 0.9)

export const lighter = 'rgba(0, 0, 0, 0.2)'
export const light = 'rgba(0, 0, 0, 0.4)'
export const simiDark = 'rgba(0, 0, 0, 0.6)'
export const dark = 'rgba(0, 0, 0, 0.8)'
