export const spaces = {
  xs: 7,
  sm: 12,
  md: 14,
  lg: 17,
  xl: 20,
}

export const fontSizes = {
  xs: 5 * 1.5,
  sm: 7 * 1.5,
  md: 12 * 1.5,
  lg: 15 * 1.5,
  xl: 20 * 1.5,
}

export const radiuses = {
  xs: 2,
  sm: 4,
  md: 6,
  lg: 8,
  xl: 10,
  straight: 0,
  round: 5,
  fullRound: 1000,
}

export const widths = {
  xs: 300,
  sm: 500,
  md: 700,
  lg: 900,
  xl: 1200,
}
