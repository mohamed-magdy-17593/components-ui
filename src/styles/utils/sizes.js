// import {css} from '@emotion/core'

// css({borderRadius})

// width
export const w = width => ({width})
export const themeW = type => ({theme}) => ({width:  theme.sizes.widths[type]})

// min width
export const maxw = type => ({theme}) => ({maxWidth: theme.sizes.widths[type]})

// font size
export const fs = size => ({theme}) => ({fontSize: theme.sizes.fontSizes[size]})

// border Radius
export const rs = size => ({theme}) => ({borderRadius: theme.sizes.radius[size]})

// border size
export const bw = width =>  ({borderWidth: width})

// padding
export const pt = size => ({theme}) => ({paddingTop: theme.sizes.spaces[size]})
export const pb = size => ({theme}) => ({paddingBottom: theme.sizes.spaces[size]})
export const pl = size => ({theme}) => ({paddingLeft: theme.sizes.spaces[size]})
export const pr = size => ({theme}) => ({paddingRight: theme.sizes.spaces[size]})
export const px = size => [pl(size), pr(size)]
export const py = size => [pt(size), pb(size)]
export const  p = size => [px(size), py(size)]

// margin
export const mt = size => ({theme}) => ({marginTop: theme.sizes.spaces[size]})
export const mb = size => ({theme}) => ({marginBottom: theme.sizes.spaces[size]})
export const ml = size => ({theme}) => ({marginLeft: theme.sizes.spaces[size]})
export const mr = size => ({theme}) => ({marginRight: theme.sizes.spaces[size]})
export const mx = size => [ml(size), mr(size)]
export const my = size => [mt(size), mb(size)]
export const  m = size => [mx(size), my(size)]

// radius
export const br = size => ({theme}) => ({borderRadius: theme.sizes.radiuses[size]})
