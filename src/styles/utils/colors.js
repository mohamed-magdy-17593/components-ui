// import {css} from '@emotion/core'

// css({})

export const fc = type => ({theme}) => ({color: theme.colors[type]})

export const bgc = type => ({theme}) => ({backgroundColor: theme.colors[type]})

export const bc = type => ({theme}) => ({borderColor: theme.colors[type]})
